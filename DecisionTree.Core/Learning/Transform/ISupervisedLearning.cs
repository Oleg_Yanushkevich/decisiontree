﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Spurt.Learning.Transform
{
    interface ISupervisedLearning<out TModel, in TInput, in TOutput> where TModel : ITransform<TInput, TOutput>
    {
        TModel Learn(TInput[] input, TOutput[] output, double[] weights = null);

        CancellationToken Token { get; set; }
    }
}
