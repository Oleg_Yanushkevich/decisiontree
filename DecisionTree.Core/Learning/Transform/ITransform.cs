﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Spurt.Learning.Transform
{
    public interface ITransform
    {
        int NubmerOfInputs { get; }
        int NumberOfOutpts { get; }
    }

    public interface ITransform<in TInput, TOutput> : ITransform
    {
        TOutput Transform(TInput input);
        TOutput[] Transform(TInput[] input);
        TOutput[] Transform(TInput[] input, TOutput[] result);
    }
}
