﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Spurt.Learning.Transform
{
    public class ParallelLearningBase
    {
        public ParallelLearningBase() { ParallelOptions = new ParallelOptions(); }

        public ParallelOptions ParallelOptions { get; set; }
        public CancellationToken Token
        {
            get { return ParallelOptions.CancellationToken; }
            set { ParallelOptions.CancellationToken = value; }
        }
    }
}
