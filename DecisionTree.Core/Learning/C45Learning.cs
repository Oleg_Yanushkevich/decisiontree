﻿using Spurt.DecisionTrees;
using Spurt.Learning.Transform;
using Spurt.Ranges;
using Spurt.Tools;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Spurt.Learning
{
    public class C45Learning : ParallelLearningBase, ISupervisedLearning<DecisionTree, double[], int>
    {
        private IList<DecisionAttribute> _attributes;
        private IntRange[] _inputRanges;

        private int _inputVariables;
        private int _outputClasses;

        private int _join;
        private int _maxHeight;

        private int _splitStep;

        private int[] _attributeUsageCount;
        private double[][] _thresholds;

        private DecisionTree _tree;

        public IList<DecisionAttribute> Attributes
        {
            get { return _attributes; }
            set { _attributes = value; }
        }
        public int Join
        {
            get { return _join; }
            set
            {
                if (value <= 0)
                    throw new ArgumentOutOfRangeException("value", "The numbers of times must be greater than zero");

                _join = value;
            }
        }
        public int MaxHeight
        {
            get { return _maxHeight; }
            set
            {
                if (value <= 0)
                    throw new ArgumentOutOfRangeException("value", "The height must be greater than zero");

                _maxHeight = value;
            }
        }
        public int SplitStep
        {
            get { return _splitStep; }
            set
            {
                if (value <= 0)
                    throw new ArgumentOutOfRangeException("value", "The split step must be greater than zero");

                _splitStep = value;
            }
        }
        public int MaxVariables { get; set; }


        public C45Learning()
        {
            _join = 1;
            _splitStep = 1;
            ParallelOptions = new ParallelOptions();
        }
        public C45Learning(DecisionAttribute[] attributes)
            : this()
        {
            Attributes = new List<DecisionAttribute>(attributes);
        }
        public C45Learning(DecisionTree tree) : this() { _init(tree); }

        public DecisionTree Learn(double[][] input, int[] output, double[] weights = null)
        {
            if (_tree == null)
            {
                if (Attributes == null)
                    Attributes = DecisionAttribute.FromData(input);

                int classes = output.Max() + 1;
                _init(new DecisionTree(Attributes, classes));
            }

            run(input, output);
            return _tree;
        }
        public DecisionTree Learn(int[][] input, int[] output, double[] weights = null)
        {
            if (_tree == null)
            {
                DecisionAttribute[] inputs = DecisionAttribute.FromData(input);
                int classes = output.Max() + 1;
                _init(new DecisionTree(inputs, classes));
            }

            run(input.ToDouble(), output);
            return _tree;
        }

        private void _init(DecisionTree tree)
        {
            if (tree == null)
                throw new ArgumentNullException("Tree can't will be a null");

            _tree = tree;
            _inputVariables = tree.NubmerOfInputs;

            _outputClasses = tree.NumberOfOutpts;
            _attributeUsageCount = new int[_inputVariables];

            _inputRanges = new IntRange[_inputVariables];
            MaxHeight = _inputVariables;
            Attributes = tree.Attributes;

            for (int i = 0; i < _inputRanges.Length; i++)
                _inputRanges[i] = tree.Attributes[i].Range.ToIntRange(false);
        }
        private void _checkArgs(double[][] inputs, int[] outputs)
        {
            if (inputs == null)
                throw new ArgumentNullException("Inputs can't will be a null");

            if (outputs == null)
                throw new ArgumentNullException("Outputs can't will be a null");

            if (inputs.Length != outputs.Length)
                throw new ArgumentOutOfRangeException("outputs", "The numer of inputs vectors and output labels dosn't match");

            if (inputs.Length == 0)
                throw new ArgumentOutOfRangeException("inputs", "Training algorithm needs at least one training vector");

            for (int i = 0; i < inputs.Length; i++)
            {
                if (inputs[i] == null)
                    throw new ArgumentNullException("inputs", $"The vector at index {i} is null");

                if (inputs[i].Length != _tree.NubmerOfInputs)
                    throw new ArgumentException("inputs", $"The size of the input vector at the index {i} doesn't match the expected number of inputs of the tree." +
                        $"All input vectors for this tree must be have length {_tree.NubmerOfInputs}");

                for (int k = 0; k < inputs[i].Length; k++)
                {
                    if (_tree.Attributes[k].Nature == DecisionAttributeType.Discrete)
                    {
                        int min = (int)_tree.Attributes[k].Range.Min;
                        int max = (int)_tree.Attributes[k].Range.Max;

                        if (inputs[i][k] < min || inputs[i][k] > max)
                            throw new ArgumentOutOfRangeException("inputs", $"The input vector at position {i} contains invalid entry at column {k}." +
                                $"The value must be between the bounds specified by the decision tree attribute variables");
                    }
                }
            }

            for (int j = 0; j < outputs.Length; j++)
            {
                if (outputs[j] < 0 || outputs[j] > _tree.NumberOfOutpts)
                    throw new ArgumentOutOfRangeException("outputs", $"The output label at index {j} should be equal to or heigher than zero," +
                        "and should be lesser the number of output classes expected by the tree.");
            }
        }

        private double _computeInfoGain(double[][] input, int[] output, int attributeIndex, double entropy, out int[][] partitions, out double threshold)
        {
            threshold = 0.0;

            if (_tree.Attributes[attributeIndex].Nature == DecisionAttributeType.Discrete)
                return (entropy - _computeInfoDiscrete(input, output, attributeIndex, out partitions));

            return (entropy + _computeInfoContinuous(input, output, attributeIndex, out partitions, out threshold));
        }
        private double _computeGainRatio(double[][] input, int[] output, int attributeIndex, double entropy, out int[][] partitions, out double threshold)
        {
            double infoGain = _computeInfoGain(input, output, attributeIndex, entropy, out partitions, out threshold);
            double splitInfo = _splitInformation(output.Length, partitions);

            if (infoGain != 0.0 && splitInfo != 0.0)
                return infoGain / splitInfo;

            return 0.0;
        }
        private double _splitInformation(int samples, int[][] partitions)
        {
            double num = 0.0;
            for (int i = 0; i < partitions.Length; i++)
            {
                double a = ((double)partitions[i].Length) / (double)samples;

                if (a != 0.0)
                    num -= a * Math.Log(a, 2.0);
            }
            return num;
        }

        private double _computeInfoDiscrete(double[][] input, int[] output, int attributeIndex, out int[][] partitions)
        {
            double num = 0.0;
            IntRange range = _inputRanges[attributeIndex];

            partitions = new int[range.Length + 1][];

            for (int i = 0; i < partitions.Length; i++)
            {
                int value = range.Min + i;
                partitions[i] = input.Find(x => x[attributeIndex] == value);

                int[] values = output.Get(partitions[i], false);
                double temp = _entropy(values, _outputClasses);

                num += (((double)values.Length) / (double)output.Length) * temp;
            }

            return num;
        }
        private double _computeInfoContinuous(double[][] input, int[] output, int attributeIndex, out int[][] partitions, out double threshold)
        {
            double[] array = _thresholds[attributeIndex];
            double negativeInfinity = double.NegativeInfinity;

            if (array.Length == 0)
            {
                partitions = new int[][] { Vector.Range(input.Length) };
                threshold = double.NegativeInfinity;

                return negativeInfinity;
            }

            double firstTemp = array[0];
            partitions = null;

            List<int> first = new List<int>();
            List<int> second = new List<int>();

            List<int> values = new List<int>();
            List<int> fourth = new List<int>();

            double[] secondArray = new double[input.Length];

            for (int i = 0; i < secondArray.Length; i++)
                secondArray[i] = input[i][attributeIndex];

            for (int j = 0; j < array.Length; j += SplitStep)
            {
                double localTemp = array[j];
                first.Clear(); second.Clear(); values.Clear(); fourth.Clear();

                for (int k = 0; k < secondArray.Length; k++)
                {
                    double secondLocalTemp = secondArray[k];

                    if (secondLocalTemp <= localTemp)
                    {
                        first.Add(k);
                        values.Add(output[k]);
                    }
                    else if (secondLocalTemp > localTemp)
                    {
                        second.Add(k);
                        fourth.Add(output[k]);
                    }
                }
                double num8 = ((double)values.Count) / ((double)output.Length);
                double num9 = ((double)fourth.Count) / ((double)output.Length);
                double num10 = (-num8 * _entropy(values, _outputClasses)) + (-num9 * _entropy(fourth, _outputClasses));

                if (num10 > negativeInfinity)
                {
                    firstTemp = localTemp;
                    negativeInfinity = num10;

                    if ((first.Count > 0) && (second.Count > 0))
                        partitions = new int[][] { first.ToArray(), second.ToArray() };

                    else if (first.Count > 0)
                        partitions = new int[][] { first.ToArray() };

                    else if (second.Count > 0)
                        partitions = new int[][] { second.ToArray() };

                    else
                        partitions = new int[0][];
                }
            }
            threshold = firstTemp;

            return negativeInfinity;
        }

        private double _entropy(int[] values, int classes) => _entropy(values, 0, classes - 1);
        private double _entropy(int[] values, int startValue, int endValue)
        {
            double num = 0.0;

            for (int i = startValue; i <= endValue; i++)
            {
                int temp = 0;

                for (int j = 0; j < values.Length; j++)
                {
                    if (values[j] == i)
                        temp++;
                }
                if (temp > 0)
                {
                    double a = ((double)temp) / ((double)values.Length);
                    num -= a * Math.Log(a, 2.0);
                }
            }
            return num;
        }
        private double _entropy(IList<int> values, int classes) => _entropy(values, 0, classes - 1);
        private double _entropy(IList<int> values, int startValues, int endValue)
        {
            double num = 0.0;
            double count = values.Count;

            for (int i = startValues; i < endValue; i++)
            {
                int temp = 0;

                foreach (var item in values)
                {
                    if (item == i)
                        temp++;
                }
                if (temp > 0)
                {
                    double a = ((double)temp) / count;
                    num -= a * Math.Log(a, 2.0);
                }
            }
            return num;
        }

        private void run(double[][] inputs, int[] outputs)
        {
            _checkArgs(inputs, outputs);

            for (int i = 0; i < _attributeUsageCount.Length; i++)
                _attributeUsageCount[i] = 0;

            _thresholds = new double[_tree.Attributes.Count][];
            List<double> list = new List<double>(inputs.Length);

            for (int j = 0; j < _tree.Attributes.Count; j++)
            {
                if (_tree.Attributes[j].Nature == DecisionAttributeType.Countinuous)
                {
                    double[] source = inputs.GetColumn<double>(j, null);

                    int[] o = (int[])outputs.Clone();

                    var groupingArray =
                        (from keyValuePair in source.Select<double, KeyValuePair<double, int>>((Func<double, int, KeyValuePair<double, int>>)((value, index) => new KeyValuePair<double, int>(value, o[index])))
                         group keyValuePair.Value by keyValuePair.Key into keyValuePair
                         orderby keyValuePair.Key
                         select keyValuePair).ToArray();

                    for (int k = 0; k < (groupingArray.Length - 1); k++)
                    {
                        IGrouping<double, int> first = groupingArray[k];
                        IGrouping<double, int> second = groupingArray[k + 1];

                        double key = second.Key;
                        double temp = first.Key;

                        if (((key - temp) > 1.110223024251556E-16) && first.Union<int>(second).Count<int>() > 1)
                            list.Add((first.Key + second.Key) / 2.0);
                    }

                    _thresholds[j] = list.ToArray();
                    list.Clear();
                }
            }
            _tree.Root = new DecisionNode(_tree);
            _split(_tree.Root, inputs, outputs, 0);
        }
        private void _split(DecisionNode root, double[][] input, int[] output, int height)
        {
            int[] candidates;
            int[][][] partitions;

            double[] scores;
            double[] thresholds;

            double entropy = _entropy(output, _outputClasses);

            if (entropy == 0.0)
            {
                if (output.Length > 0)
                    root.Output = new int?(output[0]);
            }
            else
            {
                candidates = _attributeUsageCount.Find<int>(x => x < _join);

                if ((candidates.Length == 0) || ((MaxHeight > 0) && (height == MaxHeight)))
                    root.Output = new int?(output.Mode());

                else
                {
                    int num;
                    int[] numArray3;

                    double[][] numArray2;

                    if (MaxVariables > 0)
                        candidates = Vector.Sample(candidates, MaxVariables);

                    scores = new double[candidates.Length];
                    thresholds = new double[candidates.Length];

                    partitions = new int[candidates.Length][][];
                    Parallel.For(0, scores.Length, ParallelOptions,
                        i => scores[i] = _computeGainRatio(input, output, candidates[i], entropy, out partitions[i], out thresholds[i]));

                    scores.Max(out num);
                    int[][] numArray = partitions[num];

                    int index = candidates[num];
                    IntRange range = _inputRanges[index];

                    double num3 = thresholds[num];
                    _attributeUsageCount[index]++;

                    if (_tree.Attributes[index].Nature == DecisionAttributeType.Discrete)
                    {
                        DecisionNode[] children = new DecisionNode[numArray.Length];
                        for (int j = 0; j < children.Length; j++)
                        {
                            children[j] = new DecisionNode(_tree) { Parent = root, Value = new double?((double)(j + range.Min)), Comparision = ComparisonType.Equal };
                            numArray2 = input.Get(numArray[j], false);
                            numArray3 = output.Get(numArray[j], false);

                            _split(children[j], numArray2, numArray3, height + 1);
                        }

                        root.Branches.AttributeIndex = index;
                        root.Branches.AddRange(children);
                    }
                    else if (numArray.Length > 1)
                    {
                        DecisionNode[] nodeArray3 = new DecisionNode[2];
                        DecisionNode node2 = new DecisionNode(_tree)
                        {
                            Parent = root,
                            Value = new double?(num3),
                            Comparision = ComparisonType.LessThanOrEqual
                        };

                        nodeArray3[0] = node2;
                        DecisionNode node3 = new DecisionNode(_tree)
                        {
                            Parent = root,
                            Value = new double?(num3),
                            Comparision = ComparisonType.GreaterThan
                        };

                        nodeArray3[1] = node3;
                        DecisionNode[] nodeArray2 = nodeArray3;

                        numArray2 = input.Get(numArray[0], false);
                        numArray3 = output.Get(numArray[0], false);

                        _split(nodeArray2[0], numArray2, numArray3, height + 1);

                        numArray2 = input.Get(numArray[1], false);
                        numArray3 = output.Get(numArray[1], false);

                        _split(nodeArray2[1], numArray2, numArray3, height + 1);
                        root.Branches.AttributeIndex = index;
                        root.Branches.AddRange(nodeArray2);
                    }
                    else
                    {
                        numArray3 = output.Get(numArray[0], false);
                        root.Output = new int?(numArray3.Mode());
                    }
                    _attributeUsageCount[index]--;
                }
            }

        }
    }
}
