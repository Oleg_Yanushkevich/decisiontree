﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace Spurt.Ranges
{
    public struct Range : IRange<float>, IFormattable, IEquatable<Range>
    {
        private float _min;
        private float _max;

        public float Max
        {
            get { return _max; }
            set { _max = value; }
        }
        public float Min
        {
            get { return _min; }
            set { _min = value; }
        }

        public Range(float min, float max)
        {
            _min = min;
            _max = max;
        }

        public bool IsInside(double value) => (value >= Min && value <= Max);
        public bool IsInside(IntRange range) => (IsInside(range.Min) && IsInside(range.Max));
        public Range Intersection(IntRange range) => new Range(Math.Max(Min, range.Min), Math.Min(Max, range.Max));
        public IntRange ToIntRange(bool provideInnerRange)
        {
            int left;
            int right;

            if (provideInnerRange)
            {
                left = (int)Math.Ceiling(Min);
                right = (int)Math.Floor(Max);
            }
            else
            {
                left = (int)Math.Floor(Min);
                right = (int)Math.Ceiling(Max);
            }

            return new IntRange(left, right);
        }

        public string ToString(string format, IFormatProvider formatProvider) => $"[{Min.ToString(format, formatProvider)}, {Max.ToString(format, formatProvider)}]";
        public override string ToString() => $"[{Min}, {Max}]";

        public bool Equals(Range other) => this == other;
        public override bool Equals(object obj) => (obj is Range && this == (Range)obj);
        public override int GetHashCode()
        {
            int hash = 13;
            hash = (hash * 7) + Min.GetHashCode();
            hash = (hash * 7) + Max.GetHashCode();

            return hash;
        }

        public static bool operator ==(Range first, Range second) => ((first.Min == second.Min) && (first.Max == second.Max));
        public static bool operator !=(Range first, Range second) => ((first.Min != second.Min) || (first.Max != second.Max));


        public static implicit operator DoubleRange(Range range) => new DoubleRange(range.Min, range.Max);
    }
}
