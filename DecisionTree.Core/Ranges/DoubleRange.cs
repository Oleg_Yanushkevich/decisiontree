﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace Spurt.Ranges
{
    public struct DoubleRange : IRange<double>, IEquatable<DoubleRange>, IFormattable
    {
        private double _min;
        private double _max;

        public double Length => Max - Min;

        public double Min
        {
            get { return _min; }
            set { _min = value; }

        }
        public double Max
        {
            get { return _max; }
            set { _max = value; }
        }

        public DoubleRange(double min, double max)
        {
            _min = min;
            _max = max;
        }

        public bool Equals(DoubleRange other) => this == other;
        public override bool Equals(object obj) => ((obj is DoubleRange) && this == (DoubleRange)obj);

        public override int GetHashCode()
        {
            int hash = 13;
            hash = (hash * 7) + Min.GetHashCode();
            hash = (hash * 7) + Max.GetHashCode();

            return hash;
        }

        public string ToString(string format, IFormatProvider formatProvider)
            => string.Format("[{0}, {1}]", Min.ToString(format, formatProvider), Max.ToString(format, formatProvider));

        public override string ToString() => $"[{Min}, {Max}]";

        public bool IsInside(double value) => (value >= Min && value <= Max);
        public bool IsInside(DoubleRange range) => (IsInside(range.Min) && IsInside(range.Max));
        public DoubleRange Intersection(DoubleRange range) => new DoubleRange(Math.Max(Min, range.Min), Math.Min(Max, range.Max));
        public IntRange ToIntRange(bool provideInnerRange)
        {
            int left;
            int right;

            if (provideInnerRange)
            {
                left = (int)Math.Ceiling(Min);
                right = (int)Math.Floor(Max);
            }
            else
            {
                left = (int)Math.Floor(Min);
                right = (int)Math.Ceiling(Max);
            }

            return new IntRange(left, right);
        }

        public static bool operator ==(DoubleRange first, DoubleRange second) => ((first.Min == second.Min) && first.Max == second.Max);
        public static bool operator !=(DoubleRange first, DoubleRange second) => ((first.Min != second.Min) || (first.Max != second.Max));

        public static implicit operator IntRange(DoubleRange range) => new IntRange((int)Math.Ceiling(range.Min), (int)Math.Floor(range.Max));
    }
}
