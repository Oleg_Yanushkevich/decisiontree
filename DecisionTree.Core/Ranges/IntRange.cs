﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Spurt.Ranges
{
    public struct IntRange : IRange<int>, IFormattable, IEquatable<IntRange>, IEnumerable<int>
    {
        private int _min;
        private int _max;

        public int Max
        {
            get { return _max; }
            set { _max = value; }
        }
        public int Min
        {
            get { return _min; }
            set { _min = value; }
        }

        public int Length => Max - Min;

        public IntRange(int min, int max)
        {
            _min = min;
            _max = max;
        }

        public override string ToString() => $"[{Min}, {Max}]";
        public string ToString(string format, IFormatProvider formatProvider) => $"[{Min.ToString(format, formatProvider)}, {Max.ToString(format, formatProvider)}]";

        public bool Equals(IntRange other) => this == other;
        public override bool Equals(object obj) => ((obj is IntRange) && this == (IntRange)obj);

        public bool IsInside(double value) => (value >= Min && value <= Max);
        public bool IsInside(IntRange range) => (IsInside(range.Min) && IsInside(range.Max));
        public IntRange Intersection(IntRange range) => new IntRange(Math.Max(Min, range.Min), Math.Min(Max, range.Max));

        public override int GetHashCode()
        {
            int hash = 13;
            hash = (hash * 7) + Min.GetHashCode();
            hash = (hash * 7) + Max.GetHashCode();

            return hash;
        }

        public IEnumerator<int> GetEnumerator()
        {
            int min = _min;
            while (true)
            {
                if (min >= Max)
                    yield break;

                yield return min;
                min++;
            }
        }
        IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();


        public static bool operator ==(IntRange first, IntRange second) => ((first.Min == second.Min) && (first.Max == second.Max));
        public static bool operator !=(IntRange first, IntRange second) => ((first.Min != second.Min) || (first.Max != second.Max));

        public static implicit operator DoubleRange(IntRange range) => new DoubleRange(range.Min, range.Max);
    }
}
