﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Spurt.Ranges
{
    public interface IRange<T> where T : struct, IComparable<T>
    {
        T Min { get; set; }
        T Max { get; set; }
    }
}
