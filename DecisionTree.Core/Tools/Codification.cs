﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DecisionTree.Core.Tools
{
    public class Codification
    {
    }

    public abstract class ColumnOptionsBase
    {
        protected ColumnOptionsBase(string column)
        {
            ColumnName = column;
        }
        public override string ToString() => ColumnName;


        public string ColumnName { get; set; }
        public object Tag { get; set; }
    }
}
