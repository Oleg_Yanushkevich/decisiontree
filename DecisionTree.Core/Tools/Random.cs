﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Spurt.Tools
{
    public class Random
    {
        private int _inext;
        private int _inextp;

        private const int MBIG = 0x7fffffff;
        private const int MSEED = 0x9a4ec86;

        private const int MZ = 0;
        private int[] SeedArray;


        public Random() : this(Environment.TickCount) { }
        public Random(int Seed)
        {
            SeedArray = new int[0x38];
            int num4 = (Seed == -2147483648) ? 0x7fffffff : Math.Abs(Seed);
            int num2 = 0x9a4ec86 - num4;

            SeedArray[0x37] = num2;
            int num3 = 1;

            for (int i = 1; i < 0x37; i++)
            {
                int index = (0x15 * i) % 0x37;
                SeedArray[index] = num3;

                num3 = num2 - num3;
                if (num3 < 0)
                    num3 += 0x7fffffff;

                num2 = SeedArray[index];
            }

            for (int j = 1; j < 5; j++)
            {
                for (int k = 1; k < 0x38; k++)
                {
                    SeedArray[k] -= SeedArray[1 + ((k + 30) % 0x37)];
                    if (SeedArray[k] < 0)
                        SeedArray[k] += 0x7fffffff;
                }
            }
            _inext = 0;
            _inextp = 0x15;
            Seed = 1;
        }

        private double GetSampleForLargeRange()
        {
            int num = InternalSimple();
            if (InternalSimple() % 2 == 0)
                num = -num;

            return ((num + 2147483646.0) / 4294967293);
        }
        private int InternalSimple()
        {
            int inext = _inext;
            int inextp = _inextp;

            if (++inext >= 0x38)
                inext = 1;

            if (++inextp >= 0x38)
                inextp = 1;

            int num = SeedArray[inext] - SeedArray[inextp];
            if (num == 0x7fffffff)
                num--;

            if (num < 0)
                num += 0x7fffffff;

            SeedArray[inext] = num;

            _inext = inext;
            _inextp = inextp;

            return num;
        }

        public virtual int Next() => InternalSimple();
        public virtual int Next(int maxValue)
        {
            if (maxValue < 0)
                throw new ArgumentOutOfRangeException("Max value must will be greater than zero");

            return (int)(Sample() * maxValue);
        }
        public virtual int Next(int minValue, int maxValue)
        {
            if (minValue > maxValue)
                throw new ArgumentOutOfRangeException("Max value  must be greater than min value");

            long num = maxValue - minValue;
            if (num <= 0x7fffffffL)
                return (((int)(Sample() * num)) + minValue);

            return (((int)((long)(GetSampleForLargeRange() * num))) + minValue);
        }

        public virtual void NextBytes(byte[] buffer)
        {
            if (buffer == null)
                throw new ArgumentNullException("buffer");

            for (int i = 0; i < buffer.Length; i++)
                buffer[i] = (byte)(InternalSimple() % 0x100);
        }
        public virtual double NextDouble() => Sample();

        protected virtual double Sample() => (InternalSimple() * 4.6566128752457969E-10);
    }

    public static class Generator
    {
        private static readonly ThreadLocal<Random> random;
        private static int? seed;
        private static readonly object seedLock;
        private static Random source;
        private static readonly object sourceLock;

        static Generator()
        {
            source = new Random();
            sourceLock = new object();
            seedLock = new object();
            random = new ThreadLocal<Random>(new Func<Random>(create));
        }

        private static Random create()
        {
            lock (sourceLock)
            {
                if (source == null)
                    return new Random(0);

                return new Random(source.Next());
            }
        }
        public static Random Random => random.Value;
        public static int? Seed
        {
            get { return seed; }
            set
            {
                lock (seedLock)
                {
                    seed = value;
                    lock (sourceLock)
                    {
                        if (seed.HasValue)
                        {
                            if (seed.Value == 0)
                                source = null;
                            else
                                source = new Random(seed.Value);
                        }
                        else
                            source = new Random();
                    }
                }
            }
        }
    }
}
