﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Spurt.Tools
{
    public class Vector
    {
        public static int[] Range(int n)
        {
            int[] array = new int[n];

            for (int i = 0; i < array.Length; i++)
                array[i] = i;

            return array;
        }
        public static T[] Sample<T>(T[] values, int size) => values.Get(Sample(size), false);

        public static int[] Sample(int size)
        {
            Random random = Generator.Random;
            int[] items = Range(size);

            double[] keys = new double[items.Length];

            for (int i = 0; i < keys.Length; i++)
                keys[i] = random.NextDouble();

            Array.Sort(keys, items);
            return items;
        }
    }
}
