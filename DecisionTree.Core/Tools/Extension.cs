﻿using Spurt.Ranges;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Spurt.Tools
{
    public static class Extension
    {
        public static int[] Find<T>(this T[] data, Func<T, bool> predicate)
        {
            List<int> list = new List<int>();
            for (int i = 0; i < data.Length; i++)
            {
                if (predicate(data[i]))
                    list.Add(i);
            }
            return list.ToArray();
        }
        public static T[] Get<T>(this T[] source, int[] indexes, bool inPlace = false)
        {
            if (source == null)
                throw new ArgumentNullException("source can't will be a null");

            if (indexes == null)
                throw new ArgumentNullException("indexes can't will be a null");

            if (inPlace && (source.Length != indexes.Length))
                throw new ArgumentException("Source and indexes arrays must be have the same dimension for in-place operations.");

            T[] array = new T[indexes.Length];

            for (int i = 0; i < indexes.Length; i++)
            {
                int index = indexes[i];

                if (index >= 0)
                    array[i] = source[i];
                else
                    array[i] = source[source.Length + index];
            }

            if (inPlace)
            {
                for (int j = 0; j < array.Length; j++)
                    source[j] = array[j];
            }

            return array;
        }
        public static T[] GetColumn<T>(this T[][] source, int index, T[] result = null)
        {
            if (result == null)
                result = new T[source.Length];

            index = Matrix.Index(index, source.Columns());
            for (int i = 0; i < result.Length; i++)
                result[i] = source[i][index];

            return result;
        }

        public static DoubleRange GetRange(this double[] values)
        {
            double first, second;

            values.GetRange(out first, out second);

            return new DoubleRange(first, second);
        }
        public static IntRange GetRange(this int[] values)
        {
            int first, second;

            values.GetRange<int>(out first, out second);
            return new IntRange(first, second);
        }

        public static double[][] ToDouble(this int[][] value)
        {
            var result = new double[value.Length][];

            for (int i = 0; i < value.Length; i++)
            {
                result[i] = new double[value[i].Length];

                for (int j = 0; j < value[i].Length; j++)
                    result[i][j] = (double)value[i][j];
            }

            return result;
        }
        public static void GetRange<T>(this T[] values, out T min, out T max) where T : IComparable<T>
        {
            if (values.Length == 0)
            {
                T local = default(T);
                max = local;
                min = max = local;
            }
            else
            {
                min = max = values[0];
                for (int i = 1; i < values.Length; i++)
                {
                    if (values[i].CompareTo(min) < 0)
                        min = values[i];

                    if (values[i].CompareTo(max) > 0)
                        max = values[i];
                }
            }
        }

        public static int Columns<T>(this T[][] matrix)
        {
            if (matrix.Length == 0)
                return 0;

            return matrix[0].Length;
        }
        public static T Mode<T>(this T[] values)
        {
            int num;
            return values.Mode<T>(out num, false, false);
        }

        public static T Mode<T>(this T[] values, out int count, bool inPlace, bool alreadySorted = false)
        {
            if (values.Length == 0)
                throw new ArgumentException("The values vector can't be empty.", "values");

            if (values[0] is IComparable)
                return mode_sort<T>(values, inPlace, alreadySorted, out count);

            return mode_bag<T>(values, out count);
        }
        public static T Max<T>(this T[] values, out int imax) where T : IComparable<T>
        {
            imax = 0;
            T other = values[0];

            for (int i = 1; i < values.Length; i++)
            {
                if (values[i].CompareTo(other) > 0)
                {
                    other = values[i];
                    imax = i;
                }
            }
            return other;
        }

        private static T mode_sort<T>(T[] values, bool inPlace, bool alreadySorted, out int bestCount)
        {
            if (!alreadySorted)
            {
                if (!inPlace)
                    values = (T[])values.Clone();

                Array.Sort<T>(values);
            }

            T local = values[0];
            int num = 1;
            T local2 = local;

            bestCount = num;

            for (int i = 1; i < values.Length; i++)
            {
                if (local.Equals(values[i]))
                    num++;
                else
                {
                    local = values[i];
                    num = 1;
                }
                if (num > bestCount)
                {
                    bestCount = num;
                    local2 = local;
                }
            }
            return local2;
        }
        private static T mode_bag<T>(T[] values, out int bestCount)
        {
            T local = values[0];
            bestCount = 1;

            Dictionary<T, int> dictionary = new Dictionary<T, int>();
            foreach (T local2 in values)
            {
                int num;

                if (!dictionary.TryGetValue(local2, out num))
                    num = 1;
                else
                    num++;

                dictionary[local2] = num;
                if (num > bestCount)
                {
                    bestCount = num;
                    local = local2;
                }
            }
            return local;
        }
    }
}
