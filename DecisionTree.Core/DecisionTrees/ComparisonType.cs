﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Spurt.DecisionTrees
{
    public enum ComparisonType
    {
        None,
        Equal,
        NotEqual,
        GreaterThanOrEqual,
        GreaterThan,
        LessThan,
        LessThanOrEqual
    }

    public static class ComparisionTypeExtension
    {
        public static string ToSymbol(this ComparisonType type)
        {
            switch (type)
            {
                case ComparisonType.Equal:
                    return "==";
                case ComparisonType.GreaterThan:
                    return ">";
                case ComparisonType.GreaterThanOrEqual:
                    return "&#8805;";
                case ComparisonType.LessThan:
                    return "<";
                case ComparisonType.LessThanOrEqual:
                    return "&#8804;";
                case ComparisonType.NotEqual:
                    return "&#8800;";
                case ComparisonType.None:
                default:
                    return "[none]";
            }
        }
    }
}
