﻿using Spurt.Ranges;
using Spurt.Tools;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Spurt.DecisionTrees
{
    [Serializable]
    public class DecisionAttribute
    {
        private string _name;

        public string Name
        {
            get { return _name; }
            set
            {
                if (string.IsNullOrEmpty(value))
                    throw new ArgumentException("Inadmissible decision variable name");

                _name = value;
            }
        }
        public DecisionAttributeType Nature { get; set; }
        public DoubleRange Range { get; set; }

        public static DecisionAttribute Continuous(string name) => new DecisionAttribute(name, DecisionAttributeType.Countinuous);
        public static DecisionAttribute Continuous(string name, DoubleRange range) => new DecisionAttribute(name, range);
        public static DecisionAttribute Discrete(string name, IntRange range) => new DecisionAttribute(name, range);
        public static DecisionAttribute Discrete(string name, int symbols) => new DecisionAttribute(name, symbols);
        public static DecisionAttribute[] FromData(double[][] inputs)
        {
            DecisionAttribute[] array = new DecisionAttribute[inputs.Columns()];
            for (int i = 0; i < array.Length; i++)
                array[i] = new DecisionAttribute(i.ToString(), inputs.GetColumn(i, null).GetRange());

            return array;
        }
        public static DecisionAttribute[] FromData(int[][] inputs)
        {
            DecisionAttribute[] array = new DecisionAttribute[inputs.Columns()];
            for (int i = 0; i < array.Length; i++)
                array[i] = new DecisionAttribute(i.ToString(), inputs.GetColumn<int>(i, null).GetRange());

            return array;
        }

        public DecisionAttribute(string name, DoubleRange range)
        {
            Name = name;
            Range = range;
            Nature = DecisionAttributeType.Countinuous;
        }
        public DecisionAttribute(string name, IntRange range)
        {
            Name = name;
            Range = new DoubleRange(range.Min, range.Max);
            Nature = DecisionAttributeType.Discrete;
        }
        public DecisionAttribute(string name, DecisionAttributeType nature)
        {
            Name = name;
            Nature = nature;
            Range = new DoubleRange(0.0, 1.0);
        }
        public DecisionAttribute(string name, int symbols)
            : this(name, new IntRange(0, symbols - 1)) { }


        public override string ToString() => $"{Name} : {Nature} ({Range})";
    }
}
