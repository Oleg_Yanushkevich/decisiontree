﻿using Accord.Statistics.Filters;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Spurt.DecisionTrees
{
    public class DecisionNode : IEnumerable<DecisionNode>
    {
        private DecisionTree _owner;
        private DecisionNode _parent;


        public DecisionTree Owner
        {
            get { return _owner; }
            set { _owner = value; }
        }
        public DecisionNode Parent
        {
            get { return _parent; }
            set { _parent = value; }
        }
        public ComparisonType Comparision { get; set; }
        public DecisionBranchNodeCollection Branches { get; set; }

        public int Height
        {
            get
            {
                var num = 0;
                for (DecisionNode node = Parent; node != null; node = node.Parent)
                    num++;

                return num;
            }
        }
        public bool IsLeaf => Branches != null ? Branches.Count == 0 : true;
        public bool IsRoot => _parent == null;
        public double? Value { get; set; }
        public int? Output { get; set; }

        public DecisionNode(DecisionTree tree)
        {
            _owner = tree;
            Comparision = ComparisonType.None;
            Branches = new DecisionBranchNodeCollection(this);
        }

        public bool Compute(double value)
        {
            switch (Comparision)
            {
                case ComparisonType.Equal:
                    var copy = value;
                    double? nullable = Value;

                    if (copy != nullable.GetValueOrDefault())
                        return false;

                    return nullable.HasValue;
                case ComparisonType.NotEqual:
                    copy = value;
                    return copy != Value;
                case ComparisonType.GreaterThanOrEqual:
                    copy = value;
                    nullable = Value;

                    if (copy < nullable.GetValueOrDefault())
                        return false;
                    return nullable.HasValue;
                case ComparisonType.GreaterThan:
                    copy = value;
                    nullable = Value;

                    if (copy <= nullable.GetValueOrDefault())
                        return false;
                    return nullable.HasValue;
                case ComparisonType.LessThan:
                    copy = value;
                    nullable = Value;

                    if (copy >= nullable.GetValueOrDefault())
                        return false;
                    return nullable.HasValue;
                case ComparisonType.LessThanOrEqual:
                    copy = value;
                    nullable = Value;

                    if (copy > nullable.GetValueOrDefault())
                        return false;
                    return nullable.HasValue;
            }
            throw new InvalidOperationException($"Comparison type can't have {ComparisonType.None} value ");
        }
        public bool Compute(int value)
        {
            switch (Comparision)
            {
                case ComparisonType.Equal:
                    var copy = value;
                    double? nullable = Value;

                    if (copy != nullable.GetValueOrDefault())
                        return false;

                    return nullable.HasValue;
                case ComparisonType.NotEqual:
                    copy = value;
                    return copy != Value;
                case ComparisonType.GreaterThanOrEqual:
                    copy = value;
                    nullable = Value;

                    if (copy < nullable.GetValueOrDefault())
                        return false;
                    return nullable.HasValue;
                case ComparisonType.GreaterThan:
                    copy = value;
                    nullable = Value;

                    if (copy <= nullable.GetValueOrDefault())
                        return false;
                    return nullable.HasValue;
                case ComparisonType.LessThan:
                    copy = value;
                    nullable = Value;

                    if (copy >= nullable.GetValueOrDefault())
                        return false;
                    return nullable.HasValue;
                case ComparisonType.LessThanOrEqual:
                    copy = value;
                    nullable = Value;

                    if (copy > nullable.GetValueOrDefault())
                        return false;
                    return nullable.HasValue;
            }
            throw new InvalidOperationException($"Comparison type can't have {ComparisonType.None} value ");
        }

        public IEnumerator<DecisionNode> GetEnumerator()
        {
            Stack<DecisionNode> stack = new Stack<DecisionNode>(new DecisionNode[] { this });

            while (true)
            {
                if (stack.Count == 0)
                    yield break;

                DecisionNode temp = stack.Pop();
                yield return temp;

                if (temp.Branches != null)
                {
                    for (int j = temp.Branches.Count - 1; j >= 0; j--)
                        stack.Push(temp.Branches[j]);
                }
            }
        }

        IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();

        public override string ToString() => ToString(null);
        public string ToString(Codification codebook) => _toString(codebook);

        private string _toString(Codification codebook)
        {
            string first;

            if (IsRoot)
                return "Root";

            string name = Owner.Attributes[Parent.Branches.AttributeIndex].Name;

            if (string.IsNullOrEmpty(name))
                name = "x" + Parent.Branches.AttributeIndex;

            string second = Comparision.ToSymbol();

            if (((codebook != null) && Value.HasValue) && codebook.Columns.Contains(name))
                first = codebook.Translate(name, (int)Value.Value);

            else
                first = Value.ToString();

            return $"{name} {second} {first}";
        }
    }
}
