﻿using Spurt.Learning.Transform;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Spurt.DecisionTrees
{
    public class DecisionTree : ITransform<double[], int>, IEnumerable<DecisionNode>
    {
        private int _inputs;
        private int _outputs;
        private DecisionAttributeCollection _attributes;
        private DecisionNode _root;


        public DecisionAttributeCollection Attributes => _attributes;
        public DecisionNode Root
        {
            get { return _root; }
            set { _root = value; }
        }
        public int Height
        {
            get
            {
                int num = 0;
                foreach (var node in this)
                {
                    if (node.IsLeaf)
                    {
                        int height = node.Height;
                        if (height > num)
                            num = height;
                    }
                }
                return num;
            }
        }

        public int NubmerOfInputs => _inputs;
        public int NumberOfOutpts => _outputs;


        private static int _decide(IEnumerable<double> input, DecisionNode subtree)
        {
            DecisionNode temp;

            for (var node = subtree; node != null; node = temp)
            {
                if (node.IsLeaf)
                {
                    if (!node.Output.HasValue)
                        return -1;
                    return node.Output.Value;
                }
                int attrIndex = node.Branches.AttributeIndex;
                temp = null;

                foreach (var b_node in node.Branches)
                {
                    if (b_node.Compute(input.ElementAt(attrIndex)))
                    {
                        temp = b_node;
                        break;
                    }
                }
            }
            throw new InvalidOperationException("The tree is degenerated." +
                " This is often a sign that tree is expecting discrete inputs, but it was given only real values.");
        }
        private static int _decide(IEnumerable<int> input, DecisionNode subtree) => _decide(input.Cast<double>(), subtree);


        public DecisionTree(IList<DecisionAttribute> variables, int classes)
        {
            if (classes <= 0)
                throw new ArgumentOutOfRangeException("Count of classes can't will be less than zero");

            if (variables == null)
                throw new ArgumentNullException("Varibles can't will be a null reference");

            for (int i = 0; i < variables.Count; i++)
            {
                if (variables[i].Range.Length == 0.0)
                    throw new ArgumentException($"Attribute [{i}] is a constant");
            }

            _attributes = new DecisionAttributeCollection(variables);
            _inputs = variables.Count;
            _outputs = classes;
        }

        public int Decide(double[] input) => _decide(input, Root);
        public int Decide(int[] input) => _decide(input, Root);
        public int[] Decide(double[][] input, int[] result)
        {
            double[] array = new double[NubmerOfInputs];
            for (int i = 0; i < input.Length; i++)
            {
                input[i] = array;
                result[i] = Decide(array);
            }

            return result;
        }
        public int[] Decide(double[][] input) => Decide(input, new int[input.Length]);

        public IEnumerator<DecisionNode> GetEnumerator()
        {
            if (Root == null)
                goto end;

            Stack<DecisionNode> stack = new Stack<DecisionNode>();
            postSwitch:;

            if (stack.Count != 0)
            {
                var temp = stack.Pop();
                yield return temp;

                if (temp.Branches != null)
                {
                    for (int j = temp.Branches.Count - 1; j >= 0; j--)
                        stack.Push(temp.Branches[j]);
                }
                goto postSwitch;
            }
            end:;
        }
        IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();

        public int Transform(double[] input) => Decide(input);
        public int[] Transform(double[][] input) => Decide(input);
        public int[] Transform(double[][] input, int[] result) => Decide(input, result);
    }
}
