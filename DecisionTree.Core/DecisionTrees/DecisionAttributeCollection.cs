﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Spurt.DecisionTrees
{
    public class DecisionAttributeCollection : ReadOnlyCollection<DecisionAttribute>
    {
        public DecisionAttributeCollection(IList<DecisionAttribute> list) : base(list) { }
    }
}
