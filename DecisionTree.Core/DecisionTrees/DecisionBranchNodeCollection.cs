﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Spurt.DecisionTrees
{
    public class DecisionBranchNodeCollection : Collection<DecisionNode>
    {
        private DecisionNode _owner;

        public DecisionNode Owner
        {
            get { return _owner; }
            set { _owner = value; }
        }
        public int AttributeIndex { get; set; }
        public DecisionAttribute Attribute => Owner.Owner != null ? Owner.Owner.Attributes[AttributeIndex] : null;
       

        public DecisionBranchNodeCollection(DecisionNode owner) { _owner = owner; }

        public void AddRange(IEnumerable<DecisionNode> nodes)
        {
            foreach (var node in nodes)
                Add(node);
        }
    }
}
