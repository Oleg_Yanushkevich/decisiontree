﻿using Accord;
using Accord.Math;
using Accord.Statistics.Filters;
using Spurt.DecisionTrees;
using Spurt.Learning;
using Spurt.Ranges;
using System.Data;
using CsvHelper;
using System.IO;
using System.Collections.Generic;

namespace PerformanceTests
{
    class Program
    {
        static void Main(string[] args)
        {
            ICollection<string> headers = new List<string>();
            var list = new List<object[]>();

            using (var reader = new CsvReader(new StreamReader("sample.csv")))
            {

                while (reader.Read())
                {
                    headers = reader.FieldHeaders;
                    var record = new object[headers.Count];

                    for (int i = 0; i < headers.Count; i++)
                    {
                        double temp = double.NegativeInfinity;
                        if (reader.TryGetField(i, out temp))
                            record[i] = temp;

                        else
                            record[i] = reader.GetField(i);

                        list.Add(record);
                    }
                }

            }

            DataTable data = new DataTable("Mitchell's Tennis Example");

            foreach (var name in headers)
                data.Columns.Add(name);

            foreach (var item in list)
                data.Rows.Add(item);

            //data.Rows.Add(10, "Sunny", "Hot", "High", "Weak", "No");
            //data.Rows.Add(20, "Sunny", "Hot", "High", "Strong", "No");
            //data.Rows.Add(15, "Overcast", "Hot", "High", "Weak", "Yes");
            //data.Rows.Add(1, "Rain", "Mild", "High", "Weak", "Yes");
            //data.Rows.Add(5, "Rain", "Cool", "Normal", "Weak", "Yes");
            //data.Rows.Add(8, "Rain", "Cool", "Normal", "Strong", "No");
            //data.Rows.Add(7, "Overcast", "Cool", "Normal", "Strong", "Yes");
            //data.Rows.Add(8, "Sunny", "Mild", "High", "Weak", "No");
            //data.Rows.Add(9, "Sunny", "Cool", "Normal", "Weak", "Yes");
            //data.Rows.Add(10, "Rain", "Mild", "Normal", "Weak", "Yes");
            //data.Rows.Add(11, "Sunny", "Mild", "Normal", "Strong", "Yes");
            //data.Rows.Add(12, "Overcast", "Mild", "High", "Strong", "Yes");
            //data.Rows.Add(13, "Overcast", "Hot", "Normal", "Weak", "Yes");
            //data.Rows.Add(14, "Rain", "Mild", "High", "Strong", "No");

            Codification codebook = new Codification(data);

            DecisionAttribute[] myAttributes =
            {
               new DecisionAttribute("Points", new Spurt.Ranges.DoubleRange(0,20)),
               new DecisionAttribute("Outlook", 3), // 3 possible values (Sunny, overcast, rain)
               new DecisionAttribute("Temperature", 3), // 3 possible values (Hot, mild, cool)  
               new DecisionAttribute("Humidity",    2), // 2 possible values (High, normal)    
               new DecisionAttribute("Wind",        2),  // 2 possible values (Weak, strong) 
           };

            Accord.MachineLearning.DecisionTrees.DecisionVariable[] variable =
            {
               new Accord.MachineLearning.DecisionTrees.DecisionVariable("Points",new Accord.DoubleRange(0,20)),
               new Accord.MachineLearning.DecisionTrees.DecisionVariable("Outlook", 3), // 3 possible values (Sunny, overcast, rain)
               new Accord.MachineLearning.DecisionTrees.DecisionVariable("Temperature", 3), // 3 possible values (Hot, mild, cool)  
               new Accord.MachineLearning.DecisionTrees.DecisionVariable("Humidity",    2), // 2 possible values (High, normal)    
               new Accord.MachineLearning.DecisionTrees.DecisionVariable("Wind",        2)  // 2 possible values (Weak, strong) 
            };
            int classCount = 2; // 2 possible output values for playing tennis: yes or no

            Accord.MachineLearning.DecisionTrees.DecisionTree tree = new Accord.MachineLearning.DecisionTrees.DecisionTree(variable, classCount);
            Accord.MachineLearning.DecisionTrees.Learning.C45Learning teacher = new Accord.MachineLearning.DecisionTrees.Learning.C45Learning(tree);

            DecisionTree myTree = new DecisionTree(myAttributes, classCount);

            C45Learning myTeacher = new C45Learning(myTree);

            DataTable symbols = codebook.Apply(data);
            int[][] inputs = symbols.ToJagged<int>("Company", "Owner", "Capital", "Percent", "Money", "Partner", "Type", "Part");
            int[] outputs = symbols.ToArray<int>("PlayTennis");



            myTeacher.Learn(inputs, outputs);
            teacher.Learn(inputs, outputs);


            myTree.Root.ToString();
        }
    }
}
